package main.java.uk.ac.ed.s1860393.recall.lang;

import java.util.HashSet;
import java.util.Set;

public abstract class BinaryComparison extends AtomicFormula {
	
	public BinaryComparison(Term left, Term right, Type type) {
		super(left,right,type);
	}
	
	public Term getLeftTerm() {
		return this.left;
	}

	public Term getRightTerm() {
		return this.right;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s", left, this.getType().getConnective(), right);
	}

}
