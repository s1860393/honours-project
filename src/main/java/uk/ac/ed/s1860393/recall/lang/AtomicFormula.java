package main.java.uk.ac.ed.s1860393.recall.lang;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AtomicFormula extends Formula{

    protected String name;
    protected List<Term> terms;
    protected Term left;
    protected Term right;

    // Constructor for predicate
    public AtomicFormula(String name, List<Term> terms){
        super(Formula.Type.PREDICATE);
        this.name = name;
        this.terms = new ArrayList<>();
        this.terms.addAll(terms);
        this.left = null;
        this.right = null;
    }

    // Constructor for comparsions
    public AtomicFormula(Term left, Term right, Formula.Type type){
        super(type);
        this.left = left;
        this.right = right;
        this.name = "";
        this.terms = null;
    }

    @Override
    public Set<Term> free() {
        Set<Term> free = new HashSet<>();
        if(name.isBlank()){
            if (left.isVariable()) {
                free.add(left);
            }
            if (right.isVariable()) {
                free.add(right);
            }
        } else {
            for (Term t : terms) {
                if (t.isVariable()) {
                    free.add(t);
                }
            }
        }
        return free;
    }
}
