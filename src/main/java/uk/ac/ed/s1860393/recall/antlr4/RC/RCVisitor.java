// Generated from E:/CodingWorkspace/recall/src/main/antlr4/uk/ac/ed/s1860393/recall/parsing\RC.g4 by ANTLR 4.9.1
package main.java.uk.ac.ed.s1860393.recall.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link RCParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface RCVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link RCParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(RCParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RCParser#variableList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableList(RCParser.VariableListContext ctx);
	/**
	 * Visit a parse tree produced by {@link RCParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(RCParser.ConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link RCParser#predicateName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicateName(RCParser.PredicateNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RCParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(RCParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link RCParser#termList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTermList(RCParser.TermListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParenthesizedFormula}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthesizedFormula(RCParser.ParenthesizedFormulaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LessThan}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessThan(RCParser.LessThanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Disjunction}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDisjunction(RCParser.DisjunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code UniversalQuantifier}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUniversalQuantifier(RCParser.UniversalQuantifierContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Negation}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegation(RCParser.NegationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExistentialQuantifier}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExistentialQuantifier(RCParser.ExistentialQuantifierContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Conjunction}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConjunction(RCParser.ConjunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Equality}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquality(RCParser.EqualityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Implication}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplication(RCParser.ImplicationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Predicate}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(RCParser.PredicateContext ctx);
}