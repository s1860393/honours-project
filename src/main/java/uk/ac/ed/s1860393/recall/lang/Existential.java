package main.java.uk.ac.ed.s1860393.recall.lang;

import java.util.ArrayList;
import java.util.List;

public class Existential extends QuantifiedFormula {
	public Existential(List<Term> terms, Formula operand) {
		super(terms, operand, Formula.Type.EXISTENTIAL);
	}

	@Override
	public Formula validRename(Term x, Term y) {
		List<Term> newTerms = new ArrayList<Term>();
		for (Term t : terms) {
			if (t.equals(x)) {
				newTerms.add(new Term(y.getValue(), y.isConstant()));
			} else {
				newTerms.add(new Term(t.getValue(), t.isConstant()));
			}
		}
		return new Existential(newTerms, operand.validRename(x, y));
	}
}
