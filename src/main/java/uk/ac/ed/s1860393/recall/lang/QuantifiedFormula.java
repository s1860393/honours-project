package main.java.uk.ac.ed.s1860393.recall.lang;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class QuantifiedFormula extends Formula {
    protected Formula operand;
    protected List<Term> terms;

    public QuantifiedFormula(List<Term> terms,Formula operand,Formula.Type type){
        super(type);
        this.operand = operand;
        this.terms = new ArrayList<>();
        this.terms.addAll(terms);
    }

    public Formula getOperand() {
        return this.operand;
    }

    public List<Term> getTerms() {
        return this.terms;
    }

    @Override
    public String toString() {
        List<String> list = new ArrayList<>();
        for (Term t : terms) {
            list.add(t.toString());
        }
        return String.format("%s%s( %s )", this.getType().getConnective(), String.join(",", list), operand.toString());
    }

    public Set<Term> free() {
        Set<Term> free = new HashSet<>();
        free.addAll(operand.free());
        for (Term t : terms) {
            free.remove(t);
        }
        return free;
    }
}
