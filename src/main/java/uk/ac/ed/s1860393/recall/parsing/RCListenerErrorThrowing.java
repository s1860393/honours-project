package main.java.uk.ac.ed.s1860393.recall.parsing;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/*
    Eorror Throwing class for RC listener
 */
public class RCListenerErrorThrowing extends BaseErrorListener{
    public static final RCListenerErrorThrowing INSTANCE = new RCListenerErrorThrowing();

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
                            String msg, RecognitionException e) {
        throw new RuntimeException(e);
    }
}