// Generated from E:/CodingWorkspace/recall/src/main/antlr4/uk/ac/ed/s1860393/recall/parsing\RC.g4 by ANTLR 4.9.1
package main.java.uk.ac.ed.s1860393.recall.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RCParser}.
 */
public interface RCListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RCParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(RCParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RCParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(RCParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RCParser#variableList}.
	 * @param ctx the parse tree
	 */
	void enterVariableList(RCParser.VariableListContext ctx);
	/**
	 * Exit a parse tree produced by {@link RCParser#variableList}.
	 * @param ctx the parse tree
	 */
	void exitVariableList(RCParser.VariableListContext ctx);
	/**
	 * Enter a parse tree produced by {@link RCParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(RCParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link RCParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(RCParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link RCParser#predicateName}.
	 * @param ctx the parse tree
	 */
	void enterPredicateName(RCParser.PredicateNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RCParser#predicateName}.
	 * @param ctx the parse tree
	 */
	void exitPredicateName(RCParser.PredicateNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RCParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(RCParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link RCParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(RCParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link RCParser#termList}.
	 * @param ctx the parse tree
	 */
	void enterTermList(RCParser.TermListContext ctx);
	/**
	 * Exit a parse tree produced by {@link RCParser#termList}.
	 * @param ctx the parse tree
	 */
	void exitTermList(RCParser.TermListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenthesizedFormula}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterParenthesizedFormula(RCParser.ParenthesizedFormulaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenthesizedFormula}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitParenthesizedFormula(RCParser.ParenthesizedFormulaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LessThan}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterLessThan(RCParser.LessThanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LessThan}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitLessThan(RCParser.LessThanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Disjunction}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterDisjunction(RCParser.DisjunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Disjunction}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitDisjunction(RCParser.DisjunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UniversalQuantifier}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterUniversalQuantifier(RCParser.UniversalQuantifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UniversalQuantifier}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitUniversalQuantifier(RCParser.UniversalQuantifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Negation}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterNegation(RCParser.NegationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Negation}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitNegation(RCParser.NegationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExistentialQuantifier}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterExistentialQuantifier(RCParser.ExistentialQuantifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExistentialQuantifier}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitExistentialQuantifier(RCParser.ExistentialQuantifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Conjunction}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterConjunction(RCParser.ConjunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Conjunction}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitConjunction(RCParser.ConjunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Equality}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterEquality(RCParser.EqualityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Equality}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitEquality(RCParser.EqualityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Implication}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterImplication(RCParser.ImplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Implication}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitImplication(RCParser.ImplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Predicate}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(RCParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Predicate}
	 * labeled alternative in {@link RCParser#formula}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(RCParser.PredicateContext ctx);
}