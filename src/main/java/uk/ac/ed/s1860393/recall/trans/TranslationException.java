package main.java.uk.ac.ed.s1860393.recall.trans;

public class TranslationException extends Exception {

	public TranslationException(String message) {
		super(message);
	}

}
